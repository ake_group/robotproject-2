/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.robotproject2;

/**
 *
 * @author Acer
 */
public class Robot {
    private int x;
    private int y;
    private char symbo1;
    private TableMap map;
    
    public Robot(int x, int y, char symbo1, TableMap map){
        this.x = x;
        this.y = y;
        this.symbo1 = symbo1;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbo1() {
        return symbo1;
    }
    public boolean walk(char direction){
        switch (direction){
            case 'N' :
            case 'w' :
                if(map.inmap(x, y-1)){
                    y=y-1;
                }else {
                    return false;
                }
                break;
            case 'S' :
            case 's' :   
                if(map.inmap(x, y+1)){
                    y=y+1;   
                }else {
                    return false;
                }break;
            case 'E' :
            case 'd' :
                if(map.inmap(x+1, y)){
                    x=x+1;
                }else {
                    return false;
                }break;
            case 'W' :
            case 'a' :
                if(map.inmap(x-1, y-1)){
                    x=x-1;
                }else {
                    return false;
                }break;
            default:
                return false;
              
                    
                }  if(map.isBomb(x, y)){
                    System.out.println("I'm died T^T");
        }
        return true;
    }
    public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }
}
