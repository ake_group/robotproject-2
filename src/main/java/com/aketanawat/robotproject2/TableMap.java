/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.robotproject2;

/**
 *
 * @author Acer
 */
class TableMap {
    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    
    public TableMap(int width,int height){
        this.width = width;
        this.height = height;
        
    }
        public void Showmap(){
            Title();
            for(int y = 0; y<height ; y++){
                for(int x = 0 ;x < width; x++){
                    if(robot.isOn(x,y)){
                        ShowRobot();
                    }else if (bomb.isOn(x, y))  {
                        ShowBomb();
                    }else {
                       ShowLine(); 
                    }
                   
                }IN();
            }
        }

    private void Title() {
        System.out.println("Map");
    }

    private void ShowBomb() {
        System.out.print(bomb.getSymbo1());
    }

    private void IN() {
        System.out.println("");
    }

    private void ShowLine() {
        System.out.print("-");
    }

    private void ShowRobot() {
        System.out.print(robot.getSymbo1());
    }
            public boolean inmap(int x , int y){
                return x<width && x >=0 && y>=0 && y<height;
            }
            public boolean isBomb(int x , int y){
                return bomb.isOn(x, y);
            }
             public void setBomb(Bomb bomb){
                this.bomb = bomb;
             }
             public void setRobot(Robot robot){
                this.robot = robot;
             }
}
